﻿using System;
using System.Collections.Generic;

namespace Calculator.Implementation
{
    public class ForImplementation : BaseImplementation
    {
        protected override void DoAggregation()
        {
            //var enumerator = this.Numbers.GetEnumerator();
                var list = new List<int>(this.Numbers);
            
           
                for (int i = 0; i < list.Count; i++)
                {
                this.Total += list[i];
            }
        }
    }
}


/*protected override void DoAggregation()
{
    foreach (var number in this.Numbers)
    {
        this.Total += number;
    }
    
    for (int i = 1; i <= 5; i++)
        {
            Console.WriteLine(i);
        }
    
    
    */
