﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Client
    {
        public string name { get; set; }
        public string lastName { get; set; }
        public List<int> phones { get; set; }
        public int id { get; set; }
    }


    public class invoice
    {
        public List<int> lines { get; set; }
        public Client client { get; set; }
        public Currency currency { get; set; }
        public int year { get; set; }
    }

    public class Line
    {
        public int ProductId { get; set; }
        public int value { get; set; }
    }

    public class Currency
    {
        public int idCurrency { get; set; }
        public string description { get; set; }
    }

    public class Product
    {
        public int ProductId { get; set; }
        public int ActualValue { get; set; }
        private string Name { get; set; }
        public string description { get; set; }
    }
        
    



}
